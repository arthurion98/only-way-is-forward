﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : MonoBehaviour {

    public GameObject player;
    public Level level;

    private Vector2 velocity;

    public float smoothTimeX;
    public float smoothTimeZ;

    // Use this for initialization
    void Start ()
    {

	}

    // Update is called once per frame
    void Update()
    {
        if (level.IsEditorMode())
        {
            float x = transform.position.x;
            float directionX = Input.GetAxisRaw("Horizontal");

            float z = transform.position.z;
            float directionZ = Input.GetAxisRaw("Vertical");

            Vector3 direction = new Vector3(directionX, directionZ, 0).normalized;
            transform.Translate(12f * Time.deltaTime * direction);
        }
        else
        {
            if (Level.hasLevelStarted)
            {
                float posX = Mathf.SmoothDamp(transform.position.x, player.transform.position.x, ref velocity.x, smoothTimeX * Time.deltaTime);
                float posZ = Mathf.SmoothDamp(transform.position.z, player.transform.position.z, ref velocity.y, smoothTimeZ * Time.deltaTime);

                transform.position = new Vector3(posX, transform.position.y, posZ);
            }
            else
            {
                if (Input.GetKeyDown(Menu.boutonMouvement))
                {
                    Level.StartLevel();

                    transform.position = new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z);
                }
                else
                {
                    float distance = new Vector3(transform.position.x - player.transform.position.x, 0, transform.position.z - player.transform.position.z).magnitude;
                    float posX = Mathf.SmoothDamp(transform.position.x, player.transform.position.x, ref velocity.x,3* Time.deltaTime*distance);
                    float posZ = Mathf.SmoothDamp(transform.position.z, player.transform.position.z, ref velocity.y,3* Time.deltaTime*distance);

                    if (distance <= 0.1f)
                    {
                        transform.position = new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z);
                        Level.StartLevel();
                    }
                    else
                    {
                        transform.position = new Vector3(posX, transform.position.y, posZ);
                    }
                }
            }
        }
    }
}
