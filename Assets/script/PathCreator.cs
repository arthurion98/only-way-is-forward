﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathCreator : MonoBehaviour {
    
    public Camera mainCamera;
    public Level level;
    private int mask;

    private Vector3 getWorldMousePosition()
    {
        Vector3 worldMousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        RaycastHit hit;
        Ray ray = new Ray(worldMousePosition, new Vector3(0, -1, 0));
        Physics.Raycast(ray, out hit, Mathf.Infinity, mask);

        if(hit.collider != null)
        {
            worldMousePosition = hit.point;
        }

        return worldMousePosition;
    }

	// Use this for initialization
	void Start ()
    {
        mask = (1 << LayerMask.NameToLayer("ground"));
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (level.IsEditorMode())
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (!Level.IsWaypointsCompleted())
                {
                    Level.addWaypoint(getWorldMousePosition());
                }
            }

            if (Input.GetMouseButtonDown(1))
            {
                Level.removeWaypoint();
            }

            if (Input.GetKeyDown(KeyCode.Return))
            {
                level.completeWaypoints();
            }

            if (Input.GetKeyDown(KeyCode.A))
            {
                level.savePath();
            }
        }
	}
}
