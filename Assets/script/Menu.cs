﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using UnityEngine.SceneManagement;



public class Menu : MonoBehaviour
{
    public Musique musiqueManager;

    public Button start;
    public Button options;
    public Button back;
    public Button commande;

    public Slider VolumeMusique;
    public Text TexteVolumeMusique;
    public Slider VolumeSon;
    public Text TexteVolumeSon;

    public Text texte1;
    public Text texte2;
    public Text texte3;

    public static KeyCode boutonMouvement = KeyCode.Space;

    private bool isChangingCommand;

    // Use this for initialization
    void Start()
    {
        boutonMouvement = KeyCode.Space;
        commande.GetComponent<Image>().color = Color.yellow;
        commande.GetComponentInChildren<Text>().text = "Space";

        start.onClick.AddListener(() =>
        {
            //options.gameObject.SetActive(false);
            //start.gameObject.SetActive(false);

            //Lance le niveau
            SceneManager.LoadSceneAsync("level0");
        });

        options.onClick.AddListener(() =>
        {
            options.gameObject.SetActive(false);
            start.gameObject.SetActive(false);

            back.gameObject.SetActive(true);
            VolumeMusique.gameObject.SetActive(true);
            TexteVolumeMusique.gameObject.SetActive(true);
            VolumeSon.gameObject.SetActive(true);
            TexteVolumeSon.gameObject.SetActive(true);
            commande.gameObject.SetActive(true);
            texte1.gameObject.SetActive(true);
            texte2.gameObject.SetActive(true);
            texte3.gameObject.SetActive(true);
        });

        commande.onClick.AddListener(() =>
        {
            isChangingCommand = true;
            commande.GetComponent<Image>().color = Color.green;
        });

        back.onClick.AddListener(() =>
        {
            options.gameObject.SetActive(true);
            start.gameObject.SetActive(true);

            back.gameObject.SetActive(false);
            VolumeMusique.gameObject.SetActive(false);
            TexteVolumeMusique.gameObject.SetActive(false);
            VolumeSon.gameObject.SetActive(false);
            TexteVolumeSon.gameObject.SetActive(false);
            commande.gameObject.SetActive(false);
            texte1.gameObject.SetActive(false);
            texte2.gameObject.SetActive(false);
            texte3.gameObject.SetActive(false);
            isChangingCommand = false;
            commande.GetComponent<Image>().color = Color.yellow;
        });

        VolumeMusique.onValueChanged.AddListener((volume) => {
            TexteVolumeMusique.text = Mathf.RoundToInt(volume * 100) + "%";
            musiqueManager.setVolumeMusique(volume);
        });
        VolumeSon.onValueChanged.AddListener((volume) => {
            TexteVolumeSon.text = Mathf.RoundToInt(volume * 100) + "%";
            musiqueManager.setVolumeSon(volume);
        });

        VolumeMusique.value = 0.4f;
        VolumeSon.value = 0.33f;
    }

    // Update is called once per frame
    void Update()
    {
        if(isChangingCommand && Input.anyKey)
        {
            isChangingCommand = false;
            foreach (KeyCode k in System.Enum.GetValues(typeof(KeyCode)))
            {
                if (Input.GetKey(k))
                {
                    boutonMouvement = k;
                    commande.GetComponentInChildren<Text>().text = k.ToString();
                    break;
                }
            }
            commande.GetComponent<Image>().color = Color.yellow;
        }
    }
}
