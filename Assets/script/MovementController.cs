﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour {

    public float speed;

    private Vector3 direction;
    private Vector3 directionNormalised;
    private Vector3 lastWaypoint;

    private Animator anim;
    private SpriteRenderer sprite;

    private Musique musique;

    public Level level;

    public void setStartWaypoint()
    {
        lastWaypoint = Level.getCurrentWaypoint();
        transform.position = lastWaypoint;

        updateDirection();
    }

    public void move()
    {
        float distance = getRemainingDistance();
        float speedMovement = speed * Time.deltaTime;
        if (Vector3.Dot(direction, getLocalDirection()) < 0) //overshot
        {
            updateWaypoint();
            move(speedMovement + distance);
        }
        else
        {
            if (speedMovement < distance)
            {
                move(speedMovement);
            }
            else
            {
                updateWaypoint();

                move(speedMovement - distance);
            }
        }
    }

    private Vector3 getLocalDirection()
    {
        return Level.getNextWaypoint() - transform.position;
    }

    private void move(float distance)
    {
        transform.position += distance * directionNormalised;
    }

    private void updateWaypoint()
    {
        lastWaypoint = Level.getNextWaypoint();
        Level.ChangeToNextWaypoint();

        transform.position = lastWaypoint;

        updateDirection();
    }

    private void updateDirection()
    {
        direction = Level.getDirection();
        directionNormalised = direction.normalized;
    }

    private float getRemainingDistance()
    {
        return (transform.position - Level.getNextWaypoint()).magnitude;
    }

	// Use this for initialization
	void Start ()
    {
        anim = GetComponentInChildren<Animator>();
        sprite = GetComponentInChildren<SpriteRenderer>();
        musique = GameObject.Find("Musique manager").GetComponent<Musique>();
	}

    void OnCollisionEnter(Collision collision)
    {
        //Debug.Log(collision.gameObject.name);
        if (collision.gameObject.name.Equals("Trigger"))
        {
            //Debug.Log(sprite.flipX);
            sprite.flipX = false;
        }
    }
    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.name.Equals("Trigger"))
        {
            //Debug.Log(sprite.flipX);
            sprite.flipX = true;
        }
    }

    // Update is called once per frame
    void Update ()
    {
        if (Level.IsWaypointsCompleted() && Level.hasLevelStarted)
        {
            if(Input.GetKey(Menu.boutonMouvement))
            {
                move();

                anim.SetTrigger("move");
            }
            else
            {

                anim.SetTrigger("stop");
            }
        }

        if (Input.GetKeyDown(Menu.boutonMouvement))
        {
            //anim.SetTrigger("move");
            musique.PlaySound("walk");
        }

        if (Input.GetKeyUp(Menu.boutonMouvement))
        {
            //anim.SetTrigger("stop");
            musique.StopSound();
        }
    }
}
