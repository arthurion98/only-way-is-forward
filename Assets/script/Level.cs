﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Level : MonoBehaviour {

    public string nameLevel;
    public bool editorMode;

    public Color lineColor;
    public GameObject allEnnemi;
    public static GameObject allEnnemis;
    
    private static Musique musique;

    private static LinkedList<Vector3> waypoints;
    private static LinkedListNode<Vector3> currentWaypoint;

    private static bool waypointsComplete;

    public MovementController player;
    public GameObject Player;
    public LineRenderer lineRenderer;
    public static GameObject staticPlayer;
    public static bool hasLevelStarted;
    public static bool hasLevelEnded;

    private static float y = 0.9f;

    public static void StartLevel()
    {
        hasLevelStarted = true;
    }

    public static bool IsWaypointsCompleted()
    {
        return waypointsComplete;
    }

    public void completeWaypoints()
    {
        currentWaypoint = waypoints.First;
        waypointsComplete = true;
        player.setStartWaypoint();
    }

    public static void addWaypoint(Vector3 waypoint)
    {
        waypoints.AddLast(waypoint);
    }

    public static void removeWaypoint()
    {
        if (waypoints.Count > 0)
        {
            waypoints.RemoveLast();
        }
    }

    public void savePath()
    {
        StreamWriter writer = new StreamWriter("C:/Users/Léonard/Desktop/" + nameLevel + ".txt", false);
        foreach (Vector3 n in waypoints)
        {
            writer.WriteLine("waypoints.AddLast(new Vector3(" + n.x + "f," + n.y + "f," + n.z + "f));");
        }
        writer.Close();
    }

    public void loadPath()
    {
        waypoints = new LinkedList<Vector3>();
        if (gameObject.scene.name == "level2")
        {
            waypoints.AddLast(new Vector3(-7.538099f, 0.5f, -30.28261f));
            waypoints.AddLast(new Vector3(-9.967851f, 0.5f, -30.28261f));
            waypoints.AddLast(new Vector3(-9.951323f, 0.5f, -21.53141f));
            waypoints.AddLast(new Vector3(-3.212049f, 0.5f, -21.53141f));
            waypoints.AddLast(new Vector3(-3.241265f, 0.5f, -15.863f));
            waypoints.AddLast(new Vector3(-0.8545275f, 0.5f, -13.45313f));
            waypoints.AddLast(new Vector3(3.906399f, 0.5f, -13.45313f));
            waypoints.AddLast(new Vector3(3.906399f, 0.5f, -5.691274f));
            waypoints.AddLast(new Vector3(-1.064486f, 0.5f, -5.691274f));
            waypoints.AddLast(new Vector3(-1.064486f, 0.5f, 0.2558303f));
            waypoints.AddLast(new Vector3(2.275843f, 0.5f, 2.00848f));
            waypoints.AddLast(new Vector3(0.2814157f, 0.5f, 6.790829f));
            waypoints.AddLast(new Vector3(-3.682005f, 0.5f, 4.992052f));
            waypoints.AddLast(new Vector3(-5.598023f, 0.5f, 9.500056f));
            waypoints.AddLast(new Vector3(-6.387233f, 0.5f, 14.46878f));
            waypoints.AddLast(new Vector3(-6.387233f, 0.5f, 34.12487f));
        }
        else if (gameObject.scene.name == "levelC")
        {
            waypoints.AddLast(new Vector3(2.637847f, 0.5f, -23.36301f));
            waypoints.AddLast(new Vector3(2.621318f, 0.5f, -20.77968f));
            waypoints.AddLast(new Vector3(4.673143f, 0.5f, -19.24043f));
            waypoints.AddLast(new Vector3(4.673143f, 0.5f, -8.506896f));
            waypoints.AddLast(new Vector3(-2.940111f, 0.5f, -5.002992f));
            waypoints.AddLast(new Vector3(-12.16782f, 0.5000001f, -0.1491486f));
            waypoints.AddLast(new Vector3(-12.18999f, 0.5000001f, 0.7996627f));
            waypoints.AddLast(new Vector3(-7.424562f, 0.5f, 3.112756f));
            waypoints.AddLast(new Vector3(4.590989f, 0.5f, 3.261455f));
            waypoints.AddLast(new Vector3(4.589382f, 0.5f, 7.503594f));
            waypoints.AddLast(new Vector3(-0.773674f, 0.5f, 7.503594f));
            waypoints.AddLast(new Vector3(-0.773674f, 0.5f, 9.281932f));
            waypoints.AddLast(new Vector3(7.967949f, 0.5f, 9.265409f));
            waypoints.AddLast(new Vector3(7.935238f, 0.5f, 13.73166f));
            waypoints.AddLast(new Vector3(0.9665871f, 0.5f, 13.69862f));
            waypoints.AddLast(new Vector3(-0.6140931f, 0.5f, 17.066f));
            waypoints.AddLast(new Vector3(-5.647838f, 0.5f, 17.11771f));
            waypoints.AddLast(new Vector3(-5.67249f, 0.5f, 18.44626f));
            waypoints.AddLast(new Vector3(2.617426f, 0.5f, 23.98972f));
            waypoints.AddLast(new Vector3(2.600897f, 0.5f, 30.86761f));
        }
        else if (gameObject.scene.name == "level1")
        {
            waypoints.AddLast(new Vector3(-3.945049f, 0.5f, -11.4639f));
            waypoints.AddLast(new Vector3(-3.962623f, 0.5f, 38.07872f));
            waypoints.AddLast(new Vector3(-8.284897f, 0.5f, 38.07872f));
            waypoints.AddLast(new Vector3(-8.284897f, 0.5f, 41.86578f));
            waypoints.AddLast(new Vector3(-4.071846f, 0.5f, 41.86578f));
            waypoints.AddLast(new Vector3(-4.071846f, 0.5f, 49.63641f));
            waypoints.AddLast(new Vector3(3.711683f, 0.5f, 49.63641f));
            waypoints.AddLast(new Vector3(3.711683f, 0.5f, 41.72619f));
            waypoints.AddLast(new Vector3(0.6071608f, 0.5f, 41.72619f));
            waypoints.AddLast(new Vector3(0.6071608f, 0.5f, 37.87663f));
            waypoints.AddLast(new Vector3(3.597244f, 0.5f, 37.87663f));
            waypoints.AddLast(new Vector3(3.597244f, 0.5f, 31.9202f));
            waypoints.AddLast(new Vector3(10.93928f, 0.5f, 31.9202f));
        }
        else if (nameLevel == "levelD")
        {
            waypoints.AddLast(new Vector3(8.336918f, 0.5f, -22.86662f));
            waypoints.AddLast(new Vector3(-1.406796f, 0.5f, -22.86662f));
            waypoints.AddLast(new Vector3(-1.406796f, 0.5f, -17.10137f));
            waypoints.AddLast(new Vector3(23.2356f, 0.5f, -17.10137f));
            waypoints.AddLast(new Vector3(23.2356f, 0.5f, -10.34248f));
            waypoints.AddLast(new Vector3(15.0701f, 0.5f, -10.34248f));
            waypoints.AddLast(new Vector3(15.08021f, 0.5f, -3.170906f));
            waypoints.AddLast(new Vector3(12.91794f, 0.5f, -3.170206f));
            waypoints.AddLast(new Vector3(11.28158f, 0.5f, 0.2994347f));
            waypoints.AddLast(new Vector3(11.28158f, 0.5f, 4.562136f));
            waypoints.AddLast(new Vector3(18.63382f, 0.5f, 4.562136f));
            waypoints.AddLast(new Vector3(18.6564f, 0.5f, 12.45364f));
            waypoints.AddLast(new Vector3(16.27163f, 0.5f, 15.14426f));
            waypoints.AddLast(new Vector3(12.72872f, 0.5f, 16.74188f));
            waypoints.AddLast(new Vector3(6.187149f, 0.5f, 16.80797f));
            waypoints.AddLast(new Vector3(6.174463f, 0.5f, 25.65992f));
        }
        else if (nameLevel == "levelE")
        {
            waypoints.AddLast(new Vector3(2.590656f, 0.5f, -9.618134f));
            waypoints.AddLast(new Vector3(2.590656f, 0.5f, -3.447173f));
            waypoints.AddLast(new Vector3(0.8416657f, 0.5f, -0.4966094f));
            waypoints.AddLast(new Vector3(0.8560433f, 0.5000001f, 1.569905f));
            waypoints.AddLast(new Vector3(3.489341f, 0.5f, 5.823952f));
            waypoints.AddLast(new Vector3(4.854162f, 0.5f, 5.867472f));
            waypoints.AddLast(new Vector3(4.897686f, 0.5000001f, 7.06426f));
            waypoints.AddLast(new Vector3(9.980456f, 0.5f, 15.48345f));
            waypoints.AddLast(new Vector3(10.44836f, 0.5f, 17.1262f));
            waypoints.AddLast(new Vector3(10.45924f, 0.5f, 19.03019f));
            waypoints.AddLast(new Vector3(8.174146f, 0.5f, 21.31497f));
            waypoints.AddLast(new Vector3(6.533886f, 0.5f, 22.01569f));
            waypoints.AddLast(new Vector3(0.113328f, 0.5f, 24.99888f));
            waypoints.AddLast(new Vector3(-1.782708f, 0.5f, 25.03152f));
            waypoints.AddLast(new Vector3(-1.924166f, 0.5f, 26.31289f));
            waypoints.AddLast(new Vector3(-3.034068f, 0.5f, 27.90136f));
            waypoints.AddLast(new Vector3(-3.04495f, 0.5f, 52.55885f));
        }
        else if(gameObject.scene.name == "level0")
        {
            waypoints.AddLast(new Vector3(-3.397891f, 0.5f, -20.34597f));
            waypoints.AddLast(new Vector3(-3.397891f, 0.5f, 62.63053f));
        }
        else if(gameObject.scene.name == "end")
        {
            waypoints.AddLast(new Vector3(-2.976099f, 0.5f, 52.75826f));
            waypoints.AddLast(new Vector3(-2.976099f, 0.5f, 76.2056f));
        }
    }

    public void drawPath()
    {
        lineRenderer.positionCount = waypoints.Count;

        lineRenderer.startColor = lineColor;
        lineRenderer.endColor = lineColor;

        int i = 0;
        foreach(Vector3 p in waypoints)
        {
            lineRenderer.SetPosition(i, new Vector3(p.x, p.y + 0.1f, p.z));
            i++;
        };
}

private static void DEBUGdrawPath()
    {
        LinkedListNode<Vector3> node = waypoints.First;
        while(node != waypoints.Last)
        {
            Debug.DrawLine(node.Value, node.Next.Value, Color.red);
            node = node.Next;
        }
    }

    public bool IsEditorMode()
    {
        return editorMode;
    }

    public static Vector3 getCurrentWaypoint()
    {
        Vector3 v = currentWaypoint.Value;
        v = new Vector3(v.x, y, v.z);
        return v;
    }

    public static Vector3 getNextWaypoint()
    {
        if(currentWaypoint != waypoints.Last)
        {
            Vector3 v = currentWaypoint.Next.Value;
            v = new Vector3(v.x, y, v.z);
            return v;
        }
        else
        {
            return getCurrentWaypoint();
        }
    }

    public static Vector3 ChangeToNextWaypoint()
    {
        if (currentWaypoint != waypoints.Last)
        {
            currentWaypoint = currentWaypoint.Next;
        }
        else
        {
            if(!hasLevelEnded)
            {
                hasLevelEnded = true;
                endLevel();
            }
        }
        return currentWaypoint.Value;
    }

    public static Vector3 getDirection()
    {
        return getNextWaypoint() - getCurrentWaypoint();
    }

    public void resetLevel()
    {
        completeWaypoints();
        foreach(Transform ennemi in allEnnemis.transform)
        {
            ennemi.GetComponent<Animator>().Rebind();
        }
    }

    public static void endLevel()
    {
        musique.StopSound();

        if(SceneManager.GetActiveScene().name == "level0")
        {
            SceneManager.LoadSceneAsync("level1");
        }
        else if (SceneManager.GetActiveScene().name == "level1")
        {
            SceneManager.LoadSceneAsync("levelC");
        }
        else if (SceneManager.GetActiveScene().name == "levelC")
        {
            SceneManager.LoadSceneAsync("level2");
        }
        else if (SceneManager.GetActiveScene().name == "level2")
        {
            SceneManager.LoadSceneAsync("levelD");
        }
        else if (SceneManager.GetActiveScene().name == "levelD")
        {
            SceneManager.LoadSceneAsync("levelE");
        }
        else if (SceneManager.GetActiveScene().name == "levelE")
        {
            SceneManager.LoadSceneAsync("end");
            musique.StopSong();
        }
        else if (SceneManager.GetActiveScene().name == "end")
        {
            
        }
    }

	// Use this for initialization
	void Start ()
    {
        hasLevelStarted = false;
        hasLevelEnded = false;

        loadPath();

        waypointsComplete = false;
        if (!IsEditorMode())
        {
            completeWaypoints();
        }

        allEnnemis = allEnnemi;
        staticPlayer = Player;
        
        musique = GameObject.Find("Musique manager").GetComponent<Musique>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        drawPath();
	}
}