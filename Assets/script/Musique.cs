﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Musique : MonoBehaviour {

    public AudioSource SourceMusique;
    public AudioSource SourceSon;

    // Use this for initialization
    void Start()
    {
        DontDestroyOnLoad(gameObject);

        PlaySong("infiltration");
        SourceMusique.loop = true;
        SourceSon.loop = true;
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator ReduceVolume()
    {


        for (int i = 0; i < 20; i++)
        {
            yield return new WaitForSeconds(0.08f);
            SourceMusique.volume -= 0.01f;

        }
    }

    public void PlaySong(string musique) //Joue une musique en particulier
    {
        AudioClip clip1 = Resources.Load<AudioClip>("sound/" + musique);
        if (clip1 == null) Debug.Log("Coucou");
        SourceMusique.clip = clip1;
        if (SourceMusique.isPlaying == false) SourceMusique.Play();
    }

    public void PlaySound(string son) //Joue un son en particulier
    {
        AudioClip clip2 = Resources.Load<AudioClip>("sound/" + son);
        SourceSon.clip = clip2;
        SourceSon.Play();
    }

    public void StopSong() //Pour arrêter la musique
    {
        SourceMusique.Stop();
    }

    public void StopSound()
    {
        SourceSon.Stop();
    }

    public void StopSongSlowly() //A utiliser pour arrêter lentement la musique 
    {
        StartCoroutine(ReduceVolume());
    }

    public void setVolumeMusique(float volume)
    {
        SourceMusique.volume = volume;
    }

    public void setVolumeSon(float volume)
    {
        SourceSon.volume = volume;
    }
}
